# Puppet Centreon

Table of Contents

- [Overview](#overview)
  - [Puppet Module](#puppet-module)
  - [Manifests](#manifests)
- [Versions](#versions)
  - [18.10.0](#18.10.0)
- [Requirements](#requirements)
  - [Operating systems](#operating-systems)
  - [Puppet Client](#puppet-client)
  - [Puppet Server](#puppet-server)
  - [Foreman](#foreman)
- [Installation](#installation)
- [Module Variables](#module-variables)
  - [Default Values](#default-values)
- [Examples](#examples)
  - [Installation](#simple-installation)
  - [Using Foreman](#using-foreman)
- [Screencasts](#screencasts)
- [License](LICENSE)

## Overview

This module is a set of scripts and code that assist in automating the installation process and initial setup of the Centreon environment. With Puppet, you have the ability to customize basic system settings and remotely deploy the entire environment.

Everything is based on a single configuration file, containing basic database access and configuration information.

It is also possible to implement and automate the installation of Plugins of the [Plugins Pack](https://documentation.centreon.com/docs/centreon/en/latest/quick_start/basic_plugins.html)

### Puppet Module

Modules are self-contained bundles of code and data with a specific directory structure. These reusable, shareable units of Puppet code are a basic building block for Puppet.

See more information: [Puppet Modules](https://puppet.com/docs/puppet/5.3/modules_fundamentals.html)

### Manifests

Centreon Web Catalog with MariaDB Server

- Disable selinux
- Add Centreon Open Source repository
- Install packages to Centreon poller
- Setup timezone to PHP
- Set custom option to MariaDB in CentOS 7
- Start and enable services necessaries
- Setup firewall rules

Centreon Poller Catalog

- Disable selinux
- Add Centreon Open Source repository
- Import keys from repository
- Install packages to Centreon poller
- Enable and try start centengine service

## Versions

### 18.10.0

Version with synchronized functions for version 18.10.0 of the Centreon

## Requirements

### Operating systems

This Module will work on the following operating systems:

- Red Hat / Centos

### Puppet Client

To use this module you need the installation of Puppet

### Puppet Server

### Foreman

## Installation

### Using GIT

```bash
wget https://github.com/centreon/centreon-iac-puppet/archive/master.zip
puppet module install master.zip
```

You should see an output like this:

```text
Notice: Preparing to install into /etc/puppetlabs/code/environments/production/modules ...
Notice: Downloading from https://forgeapi.puppet.com ...
Notice: Installing -- do not interrupt ...
/etc/puppetlabs/code/environments/production/modules
└─┬ lgcosta-centreon (v0.1.71)
  ├── crayfishx-firewalld (v3.4.0)
  ├── puppet-selinux (v1.6.1)
  └── puppetlabs-stdlib (v4.25.1)
```

### Using Puppet Forge

Comming soon

## Module Variables

Manifest: mariadb-server

- `mysql_root_password`: (string) Password to use with user root

Manifest: centreon-web

- `php_timezone`: (string)

Timezone to configuration of [PHP Timezones](http://php.net/manual/en/timezones.php)

- `mysql_hostname`: (string) Hostname or IP to use for connection with Mysql database
- `mysql_port`: (string) Port number to use for connection with Mysql database
- `mysql_root_password`: (string) Password used by user root (you can use variable from mariadb-server task)
- `mysql_centreon_db`: (string) Database name used by Centreon
- `mysql_centstorage_db`: (string) Database name used by Centreon Storage
- `mysql_centreon_username`: (string) Username used by Centreon
- `mysql_centreon_password`: (string) Password user used by Centreon
- `centreon_admin_password`: (string) Password used by user admin (main administrator user)
- `repository`: (string) Centreon repository RPM package URL (Default: `http://yum.centreon.com/standard/18.10/el7/stable/noarch/RPMS/centreon-release-18.10-2.el7.centos.noarch.rpm`)
- `plugin_pack`: (array)

A array of plugins pack to install

Valid values:

- base-generic
- Centreon
- Centreon DB
- Centreon Poller
- Cisco standard
- Linux SNMP
- MySQL DB
- Printer standard
- UPS Standard
- Windows SNMP
- DHCP Server
- DNS Service
- FTP Server
- HTTP Server
- LDAP Server
- 3com Network
- AIX SNMP
- AKCP Sensor
- Alcatel OXE
- Apache Server

### Default values

```ruby
  String $mysql_centreon_hostname    = 'localhost',
  String $mysql_centstorage_hostname = 'localhost',
  String $mysql_port                 = '3306',
  String $mysql_centreon_username    = 'centreon',
  String $mysql_centreon_db          = 'centreon',
  String $mysql_centstorage_db       = 'centreon_storage',
  String $mysql_root_password        = 'changeme',
  String $mysql_centreon_password    = 'changeme',
  String $centreon_admin_password    = 'changeme',
  String $php_timezone               = 'Europe/Paris',
  Array  $plugin_pack                = ['base-generic', 'Linux SNMP']
```

## Examples

### Simple Installation

Simple Installation using a Centos clean install and configure a new Host with Centreon

You need a installation of CentOS 7 with [Puppet Client](#puppet-client) installed

[Using Vagrant images to prepare Centreon](docs/vagrant.md)

Create a new file with deploy components for installation of Centreon Web:

```ruby
class {'centreon::mariadb':}
class {'centreon::web':
  mysql_root_password        => '123test',
  mysql_centreon_password    => '123test',
  centreon_admin_password    => '123test',
  require                    => Class['centreon::mariadb']
}
```

Change the values of variables according to your environment

Now, you can apply the file locally, using these command:

```bash
puppet apply -v centreon.pp
```
